#-------------------------------------------------
#
# Project created by QtCreator 2015-11-15T02:45:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = src
TEMPLATE = app


SOURCES += main.cpp\
        Widget.cpp \
    search.cpp

HEADERS  += Widget.h \
    search.h

FORMS    += Widget.ui

