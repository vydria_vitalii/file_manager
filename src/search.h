#ifndef SEARCH_H
#define SEARCH_H

#include <QLabel>
#include <QComboBox>
#include <QDialog>
#include <QTableWidget>
#include <QPushButton>
#include <QTableWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDir>

/**
 * Поиск файлов.
 * Реализует поиск файлов в указанном каталге с параметрами поска.
 */
class Search : public QDialog
{
    Q_OBJECT

public:
    /**
     * Конструктор с параметрами.
     * @param parent - указатель на родителя.
     */
    explicit Search(QWidget *parent = 0);
    static const int col = 4; ///< количество колонок таблицы результатов поиска

private slots:
    /**
     * Выбор каталога для поиска.
     */
    void browse();
    /**
     * Выполняем поиск.
     */
    void find();
    /**
     * Открытие элемента (находящегося в таблице).
     */
    void openFileOfItem(int row, int column);

private:
    /**
     * Искать файлы соответствующие пользовательским спецификациям.
     * @param files - результаты найденых файлов.
     * @param text - текс который должен быть в файлах.
     */
    QStringList findFiles(const QStringList &files, const QString &text);
    /**
     * Вывод на экран результатов.
     * @param files - результаты найденых файлов.
     */
    void showFiles(const QStringList &files);
    /**
     * Создание кнопки.
     * @param text - контент кпопки.
     * @param member - указатель на слот кнопки.
     */
    QPushButton *createButton(const QString &text, const char *member);
    /**
     * Создание ComboBox.
     * @param text - контент элемента ComboBox.
     */
    QComboBox *createComboBox(const QString &text = QString());
    /**
     * Создание таблицы результатов поиска.
     */
    void createFilesTable();
    QComboBox *fileComboBox; ///< указатель на QComboBox с именем файла
    QComboBox *textComboBox; ///< указатель на QComboBox с текстом для поиска в файле
    QComboBox *directoryComboBox; ///< указатель на QComboBox - путь к каталогу поиска
    QLabel *fileLabel; ///< указатель на QLabel
    QLabel *textLabel; ///< указатель на QLabel
    QLabel *directoryLabel; ///< указатель на QLabel
    QLabel *filesFoundLabel; ///< указатель на QLabel
    QPushButton *browseButton; ///< указатель кнопка обзора каталогов для поиска
    QPushButton *findButton;  ///< указатель кнопка запуска поиска
    QTableWidget *filesTable; ///< указатель на таблицу результатов поиска
    QDir currentDir; ///< объект класса QDir для работы с файловой системой.
    static const int minHeight = 300; ///< минимальные размеры высоты окна
    static const int minWidth = 600;  ///< минимальная ширина окна.
    static const int maxWidth = 1000; ///< максимальная ширина окна.
    static const int maxHeight = 600; ///< максимальная высота окна.
};

#endif // SEARCH_H
