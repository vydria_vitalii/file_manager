#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDesktopWidget>
#include <QFileSystemModel>

/**
 * Контент окна(файл: Widget.ui).
 */
namespace Ui {
class Widget;
}

/**
 * Главное окно, которое отображает список томов и их содержимое.
 * Отображает все томы и содержимое в них.
 */
class Widget : public QWidget
{
    Q_OBJECT

public:
    /**
     * Конструктор с параметрами.
     * @param parent - указатель на родителя.
     */
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui; ///< указатель на виджет окна.
    QFileSystemModel *systemModel; ///< указатель на модель представления файловой системы.
    static const int minHeight = 300; ///< минимальная высота окна.
    static const int minWidth = 400; ///< минимальная ширина окна.
private slots:
    /**
     * Слот запуска одна для поиска файлов.
     */
    void runWindowSearch();
    /**
     * Слот отображения информации об авторе проекта.
     */
    void showAboutAuthor();
};

#endif // WIDGET_H
