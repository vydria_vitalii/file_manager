#include "Widget.h"
#include <QApplication>
/**
 * @brief main
 * @param argc - количество параметрво.
 * @param argv - аргументы функции.
 * @return код возврата.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();
    return a.exec();
}
