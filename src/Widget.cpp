#include "Widget.h"
#include "ui_Widget.h"
#include <QDebug>
#include <QString>
#include "search.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setWindowTitle("File manager");
    this->setMinimumSize(minWidth,minHeight);
    connect(ui->search,SIGNAL(clicked()),this,SLOT(runWindowSearch()));
    move(QApplication::desktop()->screen()->rect().center() - rect().center()); //center window position
    systemModel = new QFileSystemModel(this);
    QFileInfoList filesInfo = QDir::drives(); // считываем локальные диски
    systemModel->setRootPath(filesInfo[0].absolutePath()); //корневой каталог для отслеживания
    ui->treeView->setModel(systemModel);
    ui->treeView->setColumnWidth(0,300);
    ui->treeView->header()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->treeView->header()->setStretchLastSection(false);
    connect(ui->aboutAuthor,SIGNAL(clicked()),this,SLOT(showAboutAuthor()));
}

Widget::~Widget()
{
    delete ui;
}
void Widget::runWindowSearch()
{
    Search *win = new Search(this);
    win->setWindowFlags(Qt::Window);
    win->show();
}

void Widget::showAboutAuthor()
{
    QMessageBox::information(this,"About Author","\tКурсовая работа по СПО\n\t\tна тему:\n\t'Файловый менеджер'\n\tВыполнил: ст.гр. КИТ-23А\n\tВыдря Виталий");
}
